from django.test import TestCase
from courses.models.course import Course
from courses.models.item import Item, Essay, TextPage, Quiz
from courses.models.module import Module


class CourseModelTestCase(TestCase):
    """
    Test case for the Course model.
    """

    def test_course_creation(self):
        """
        Test creating a Course instance.
        """
        course = Course.objects.create(
            title='Test Course',
            description='This is a test course.'
        )
        self.assertEqual(course.title, 'Test Course')
        self.assertEqual(course.description, 'This is a test course.')
        self.assertIsNotNone(course.created_at)
        self.assertIsNotNone(course.updated_at)

    def test_course_get_modules(self):
        """
        Test retrieving modules associated with a Course instance.
        """
        course = Course.objects.create(
            title='Test Course',
            description='This is a test course.'
        )
        module = Module.objects.create(
            course=course,
            title='Test Module',
            description='This is a test module.',
            order=1
        )
        self.assertIn(module, course.get_modules())


class ModuleModelTestCase(TestCase):
    """
    Test case for the Module model.
    """

    def test_module_creation(self):
        """
        Test creating a Module instance.
        """
        course = Course.objects.create(
            title='Test Course',
            description='This is a test course.'
        )
        module = Module.objects.create(
            course=course,
            title='Test Module',
            description='This is a test module.',
            order=1
        )
        self.assertEqual(module.course, course)
        self.assertEqual(module.title, 'Test Module')
        self.assertEqual(module.description, 'This is a test module.')
        self.assertEqual(module.order, 1)


class TextPageModelTestCase(TestCase):
    """
    Test case for the TextPage model.
    """

    def test_text_page_creation(self):
        """
        Test creating a TextPage instance.
        """
        course = Course.objects.create(
            title='Test Course',
            description='This is a test course.'
        )
        module = Module.objects.create(
            course=course,
            title='Test Module',
            description='This is a test module.',
            order=1
        )
        text_page = TextPage.objects.create(
            module=module,
            title='Test Text Page',
            order=1,
            content='This is a test text page content.'
        )
        self.assertEqual(text_page.module, module)
        self.assertEqual(text_page.title, 'Test Text Page')
        self.assertEqual(text_page.order, 1)
        self.assertEqual(text_page.content, 'This is a test text page content.')


class QuizModelTestCase(TestCase):
    """
    Test case for the Quiz model.
    """

    def test_quiz_creation(self):
        """
        Test creating a Quiz instance.
        """
        course = Course.objects.create(
            title='Test Course',
            description='This is a test course.'
        )
        module = Module.objects.create(
            course=course,
            title='Test Module',
            description='This is a test module.',
            order=1
        )
        quiz = Quiz.objects.create(
            module=module,
            title='Test Quiz',
            order=1,
            introduction='This is a test quiz introduction.'
        )
        self.assertEqual(quiz.module, module)
        self.assertEqual(quiz.title, 'Test Quiz')
        self.assertEqual(quiz.order, 1)
        self.assertEqual(quiz.introduction, 'This is a test quiz introduction.')


class EssayModelTestCase(TestCase):
    """
    Test case for the Essay model.
    """

    def test_essay_creation(self):
        """
        Test creating an Essay instance.
        """
        course = Course.objects.create(
            title='Test Course',
            description='This is a test course.'
        )
        module = Module.objects.create(
            course=course,
            title='Test Module',
            description='This is a test module.',
            order=1
        )
        essay = Essay.objects.create(
            module=module,
            title='Test Essay',
            order=1,
            question='This is a test essay question.'
        )
        self.assertEqual(essay.module, module)
        self.assertEqual(essay.title, 'Test Essay')
        self.assertEqual(essay.order, 1)
        self.assertEqual(essay.question, 'This is a test essay question.')
