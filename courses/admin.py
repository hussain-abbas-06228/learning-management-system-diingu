from django.contrib import admin
from courses.models.course import Course
from courses.models.item import Item, TextPage, Essay, Quiz
from courses.models.module import Module


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    """
    Admin configuration for the Course model.
    """
    list_display = ('title', 'created_at', 'updated_at')
    search_fields = ('title', 'description')
    list_filter = ('created_at', 'updated_at')


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    """
    Admin configuration for the Module model.
    """
    list_display = ('title', 'course', 'order')
    list_filter = ('course',)
    search_fields = ('title', 'description')


@admin.register(TextPage)
class TextPageAdmin(admin.ModelAdmin):
    """
    Admin configuration for the TextPage model.
    """
    list_display = ('title', 'module', 'order')
    list_filter = ('module',)
    search_fields = ('title',)


@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    """
    Admin configuration for the Quiz model.
    """
    list_display = ('title', 'module', 'order')
    list_filter = ('module',)
    search_fields = ('title',)


@admin.register(Essay)
class EssayAdmin(admin.ModelAdmin):
    """
    Admin configuration for the Essay model.
    """
    list_display = ('title', 'module', 'order')
    list_filter = ('module',)
    search_fields = ('title',)
