from django.db import models


class ItemQuerySet(models.QuerySet):
    pass


class ItemManager(models.Manager):
    pass


class Item(models.Model):
    module = models.ForeignKey('courses.Module', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    order = models.IntegerField()

    objects = ItemManager()

    class Meta:
        ordering = ['order']
        # This makes the Item model abstract so it won't create its own table in the database
        abstract = True

    def __str__(self):
        """
        Returns a string representation of the item.
        """
        return self.title


class TextPage(Item):
    content = models.TextField()

    def __str__(self):
        """
        Returns a string representation of the text page.
        """
        return f'TextPage: {self.title}'


class Quiz(Item):
    introduction = models.TextField()

    def __str__(self):
        """
        Returns a string representation of the quiz.
        """
        return f'Quiz: {self.title}'


class Essay(Item):
    question = models.TextField()

    def __str__(self):
        """
        Returns a string representation of the essay.
        """
        return f'Essay: {self.title}'
