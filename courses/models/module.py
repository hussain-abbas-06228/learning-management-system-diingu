from django.db import models


class ModuleQuerySet(models.QuerySet):
    
    def filter_by_course(self, course):
        self.filter(course=course)


class ModuleManager(models.Manager):
    pass


class Module(models.Model):
    course = models.ForeignKey('courses.Course', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    order = models.IntegerField()

    objects = ModuleManager()

    class Meta:
        ordering = ['order']

    def __str__(self):
        """
        Returns a string representation of the module.
        """
        return self.title

    def get_text_pages(self):
        """
        Retrieve all TextPage items associated with this module.
        """
        return self.textpage_set.all()

    def get_quizzes(self):
        """
        Retrieve all Quiz items associated with this module.
        """
        return self.quiz_set.all()

    def get_essays(self):
        """
        Retrieve all Essay items associated with this module.
        """
        return self.essay_set.all()
