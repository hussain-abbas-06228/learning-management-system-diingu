from django.db import models


class CourseQuerySet(models.QuerySet):
    pass


class CourseManager(models.Manager):
    pass


class Course(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = CourseManager()

    def __str__(self):
        """
        Returns a string representation of the course.
        """
        return self.title

    def get_modules(self):
        """
        Retrieve all modules associated with this course.
        """
        return self.module_set.all()
