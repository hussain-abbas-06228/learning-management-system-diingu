# Learning Management System (LMS)

 This is a Django-based Learning Management System (LMS) for managing courses, modules, and items (lessons/quizzes/assignments).

## Setup Instructions

### 1. Clone the Repository

```bash
git clone https://gitlab.com/hussain-abbas-06228/learning-management-system-diingu
cd learning_management_system
```

### 2. Create a Virtual Environment

```bash
# on Windows
python -m venv .venv
.venv/Scripts/activate
```

### 3. Install the Requirements

```bash
pip install -r requirements.txt
```

### 4. Create SuperUser

You can create a superuser to access the Django admin interface.

```bash
python manage.py createsuperuser
```

### 5. Run the Development Server

```bash
python manage.py runserver
```

The project will be available at **[http://localhost:8000/](http://localhost:8000/admin)**

### 6. Access the Django Admin


You can access the Django admin interface at **[http://localhost:8000/admin](http://localhost:8000/admin)** and log in using the superuser credentials. After logging in with your created superuser, you can create courses, modules, and items.



## Testing Models Using Django Admin

In addition to writing test cases using Django's testing framework, you can also interactively test your models using the Django admin dashboard.

### Accessing the Django Admin

1. Ensure that your Django development server is running. If not, start the server by running:

    ```bash
    python manage.py runserver
    ```

2. Open your web browser and navigate to the Django admin URL (`http://localhost:8000/admin/` by default).

3. Log in using the superuser credentials you created during the setup process.

### Testing Models

Once logged in to the Django admin, you can perform various actions to test your models:

- **Create Objects**: Use the "Add" button to create new objects for your models. Fill in the required fields and save the object.
  
- **View Objects**: Navigate to the relevant model's section to view a list of existing objects. Click on an object to view its details.

- **Update Objects**: Select an object from the list and click on the "Change" button to edit its details. Make changes as needed and save the object.

- **Delete Objects**: Select one or more objects from the list and click on the "Delete" button to remove them from the database.

By interacting with your models through the Django admin, you can quickly test their behavior and ensure that they are working as expected.


## Testing Models Using Django's built-in test suite.

To run the tests, run the following command:

```bash
python manage.py test
```

This command will run all the tests defined in the tests.py files of your Django apps. 


